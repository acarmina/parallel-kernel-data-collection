#!/bin/bash
symb=$(echo $1| cut -d, -f1);
fn=$(echo $1| cut -d, -f2);
tmp_fn="/tmp/x_${symb}_${2}.cocci"
echo "$1 -- symb=$symb, file=$fn";
for i in $(cat body.cocci.template | sed -e "s/##name##/${symb}/g" >${tmp_fn} && spatch -sp_file ${tmp_fn} /home/alessandro/src/linux-5.18.4/${fn} 2>/dev/null| spatch -sp_file  called_funcs.cocci --opt-c /dev/stdin 2>/dev/null); do
        psql -h dbs.hqhome163.com kernel -c "insert into calls (caller, callee) select (select id from symbols where symbol ='${symb}' and file='${fn}' and type='func'), id from symbols where symbol ='$i';";
        done;
rm -f ${tmp_fn}
