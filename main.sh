#!/bin/bash
[ $# -ne 2 ] && echo -e "$0 <no tasks> <File list>" && exit
echo -n $1 > /tmp/count_$$
echo "initialize count as $1 for group $$" 1>&2
for i in $(psql -t -A -F"," -h dbs.hqhome163.com kernel -c "select symbol,file from symbols  where file~'^./$2/*'"); do
	flock -w 10 -x /tmp/count_$$ ./cdec.sh count_$$ >/dev/null
        ./task.sh $i $$ &
        echo "update count $COUNT task $! is getting $i " 1>&2
        while [ $(cat /tmp/count_$$) -eq 0 ]; do
                sleep 0.1;
                done
        done
echo "List done. Wait for remaining task end" 1>&2
while [ $(cat /tmp/count_$$) -ne $1 ]; do
        sleep 1;
        done
rm -f /tmp/count_$$


